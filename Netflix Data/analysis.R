library("ggplot2")
library("dplyr")
setwd("C:\EMC\Data Science\R programming\Netflix Data")
df <- read.csv("Netflix Shows.csv", stringsAsFactors = FALSE)
str(df)

table(is.na(df$release.year))

## Check which columns has NA's
lapply(lapply(df,is.na), table)

##Find out which are NA's
which(is.na(df$user.rating.score))
df[2,]


df$rating <- as.factor(df$rating)

table(df$rating)

# ggplot(df, aes(x = rating )) + geom_histogram(stat = "count") 

releases_per_year <- df %>% select(title, release.year) %>% group_by(release.year) %>% 
  summarise( movies_per_year = n()) %>%
  arrange(desc(movies_per_year))

head(releases_per_year)


ggplot(releases_per_year, aes(x = (release.year), y = movies_per_year)) + 
  geom_bar(stat = "identity") + geom_smooth(method = "lm", se = F)





