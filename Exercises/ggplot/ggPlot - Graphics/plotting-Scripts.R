chickflick <- read.delim('./Datasets/ChickFlick.dat')

library("ggplot2")

boxplot <- ggplot(chickflick,aes(x=film,y=arousal,color = gender)) + geom_boxplot()

barchart <- ggplot(chickflick , aes(film,arousal,fill=gender)) + stat_summary(fun.y = mean,geom="bar")
barchart + xlab("Movie Name ")+ ylab("Mean Arousal") + facet_wrap(~gender)

hygine_data <- read.delim('./Datasets/DownloadFestival.dat')
library("dplyr")

hygine_data <- hygine_data %>% filter(hygine_data$day1 < 10)

hygine_chart <- ggplot(hygine_data,aes(day1,fill=hygine_data$gender)) +
  geom_histogram(binwidth = 1,position = "dodge") + xlab("Hygine Score")+ ylab("density")

hygine_chart <- ggplot(hygine_data,aes(day1,fill=hygine_data$gender)) +
  geom_histogram( binwidth = 1,position = "dodge") + xlab("Hygine Score")+ ylab("density")


hygine_chart

